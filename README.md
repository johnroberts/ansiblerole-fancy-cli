# Summary
Set up a comfortable command-line environment

# Components
- Zsh for shell
- Oh My Zsh
- Powerline font
- Yakuake (pull-down terminal emulator)
- Manage dotfiles using GNU stow + a git repo (like https://alexpearce.me/2016/02/managing-dotfiles-with-stow/)

# Supported Platforms
Tested on:
- Kubuntu 22.04
- MX Linux KDE 23

Should work (possibly with modifications) on other Debian/Ubuntu variants